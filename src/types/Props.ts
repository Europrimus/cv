import type { BASICS } from "./resume";
import type { Line } from "./Line";

export interface API {
  baseURL: URL;
}

export interface SourceRepository {
  url: URL;
  host: string;
}

export interface ResumeData {
  basics: BASICS,
  skills: {
    programming?: Line[],
    bicycle?: Line[],
    other?: Line[],
    img?: URL[],
  },
  work?: Line[],
  training?: Line[],
  interests?: Line[],
  volunteer?: Line[],
  languages?: Line[],
  projects?: Line[],
}
