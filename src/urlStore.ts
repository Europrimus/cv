import { writable } from 'svelte/store';

let persistentStorage = (function() {
    const { subscribe, set } = writable()
    interface DataStorageObject {
        [key: string]: string[]
    };
    const searchArraySep="~"
    let dataStorage: DataStorageObject = parseURL()

    function parseURL():DataStorageObject{
        const urlSearchParams = new URL(document.URL).searchParams;
        let data:DataStorageObject = decodeSearchParams(urlSearchParams);
        return data;
    }

    function updateURL():void {
        window.history.pushState(null, '', getAsURL());
        // call set() function to run subscribers
        set(dataStorage);
    }

    function hasKeyVal(key: string, value: string): boolean{
        if (dataStorage[key] !== undefined){
            return dataStorage[key].includes(value);
        }
        return false;
    }

    function add(key: string, value: string){
        if(hasKeyVal(key,value)){
            // duplicate, do nothing
        }else if(dataStorage[key] !== undefined){
            dataStorage[key].push(value);
        }else{
            dataStorage[key]=[value];
        }
        updateURL();
    }

    function remove(key: string, value: string){
        if(dataStorage[key] !== undefined){
            let values=dataStorage[key]
            const index = values.indexOf(value);
            if (index > -1) {
                // remove the value from array
                values.splice(index, 1);
            }
            dataStorage[key]=values;
            updateURL();
        }
    }

    function getAsURL():URL{
        let url = new URL(document.URL);
        url.search = encode(dataStorage);
        return url;
    }

    function decodeSearchParams(searchParams:URLSearchParams):DataStorageObject{
        let data:DataStorageObject = {};
        searchParams.forEach( (value,key)=>{
            data[key] = value.split(searchArraySep).map(function(val) {
                    return decodeURI(val).replace('%7E', searchArraySep)
                });
        })
        return data;
    }

    function encode(data:DataStorageObject):string{
        let url = new URL(document.URL);
        let search=[]
        for (let key in data) {
            if (data.hasOwnProperty(key) && data[key].length > 0) {
                let values=data[key].map(function(val) {
                    return val.replace(searchArraySep, '%7E')
                });
                search.push(key+"="+values.join(searchArraySep))
            }
        }
        url.search = encodeURI(search.join('&'))
        return url.search;
    }

    function resetFromCurrentUrl(){
        dataStorage = parseURL();
    }

    function getRawData():DataStorageObject{
        return dataStorage
    }

    return {
    subscribe,
    set,
    hasKeyVal,
    add,
    remove,
    getAsURL,
    resetFromCurrentUrl,
    getRawData, // only used for test
    }
})();

export default persistentStorage;
