import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		api: {
			baseURL: new URL('resume.json', window.location.href)
		},
		sourceRepository: {
			url: new URL('https://framagit.org/Europrimus/cv'),
			host: 'framagit.org'
		}
	}
});

export default app;
