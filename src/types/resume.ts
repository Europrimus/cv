export interface BASICS {
    name: string,
    label?: string,
    image?: URL,
    email?: string,
    phone?: string,
    url?: URL,
    summary?: string,
    location?: {
      address?: string,
      postalCode?: string,
      city?: string,
      countryCode?: string,
      region?: string
    },
    profiles?: [{
      network?: string,
      username?: string,
      url?: URL
    }]
}

export interface WORK {
    name: string,
    position: string,
    url?: URL,
    startDate: Date,
    endDate?: Date,
    summary: string,
    highlights?: string[],
}

export interface VOLUNTEER {
    organization: string,
    position?: string,
    url?: URL,
    startDate: Date,
    endDate?: Date,
    summary: string,
    highlights?: string[],
}

export interface EDUCATION {
    institution: string,
    url?: URL,
    area: string,
    studyType: string,
    startDate: Date,
    endDate?: Date,
    score?: number,
    courses: string[],
}

export interface CERTIFICATE {
    name: string,
    date: Date,
    issuer: string,
    url?: URL,
}

export interface SKILL {
    name: string,
    level: string,
    keywords: string[],
    image?: string,
    category?: string,
}

export interface LANGUAGE {
    language: string,
    fluency: string,
}

export interface INTEREST {
    name: string,
    keywords: string[],
}

export interface PROJECT {
    name: string,
    startDate: Date,
    endDate?: Date,
    description: string,
    highlights: string[],
    url: URL,
    entity?: string,
    keywords?: string,
}

export interface RESUME {
    basics: BASICS,
    work?: WORK[],
    volunteer?: VOLUNTEER[],
    education?: EDUCATION[],
    awards?: object[],
    certificates?: CERTIFICATE[],
    publications?: object[],
    skills?: SKILL[],
    languages?: LANGUAGE[],
    interests?: INTEREST[],
    references?: object[],
    projects?: PROJECT[]
}
