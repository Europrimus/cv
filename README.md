# CV powered by svelte

## Configure

you can edit `src/main.ts` to change `resume.json` and `sourceRepository` url.

### Resume

It use the standard [JSON Resume](https://jsonresume.org/).
The data come from the file ```public/resume.json```.
You can replace it with your resume.

## Starting dev

run the development server

```sh
npm run dev
```

## Build for production

```sh
npm run build
```

## Todo

- create links for ```basics.profiles```
- improve printer friendly css
- add more background images and effects
