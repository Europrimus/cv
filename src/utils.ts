import type { SKILL } from "./types/resume";

export function imgListFromSkills(skills: SKILL[]): URL[]{
  let imgs: URL[] = [];
  skills.forEach(skill => {

    if (skill.image) {
        let imgUrl:URL = new URL(skill.image);
        imgs.push(imgUrl);
    };
  });
  return imgs;
}