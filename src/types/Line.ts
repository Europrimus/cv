import type { EDUCATION, SKILL, PROJECT, LANGUAGE, VOLUNTEER, INTEREST, WORK, CERTIFICATE } from "./resume";

export interface Line {
  summary: string;
  link?: {
    name: String;
    url?: URL;
  }
  details?: string[];
  startDate?: Date;
  endDate?: Date;
  extended?: boolean;
  keywords?: string[];
  id?:string;
}

async function calcChecksum(line:Line, maxLength=6){
  const encoder = new TextEncoder();
  const data = encoder.encode(JSON.stringify(line));
  const checkSum = await crypto.subtle.digest(
    "SHA-1",
    data,
  );
  const checkSumText = Array.from( new Uint8Array(checkSum) )
    .map((b) => b.toString(16).padStart(2, "0"))
    .join("");
  // reduce length of hash for qrCode reduction
  return checkSumText.substring(checkSumText.length - maxLength, checkSumText.length);
}

export async function fromEducationAndCertificates(education: EDUCATION[], certificates: CERTIFICATE[]): Promise<Line[]>{
  let lines: Line[] = [];
  lines = lines.concat(await fromEducation(education));
  lines = lines.concat(await fromCertificates(certificates));
  return lines;
}

export async function fromEducation(education: EDUCATION[]): Promise<Line[]>{
  let lines: Line[] = [];
  for(let edu of education) {
    let line: Line = {
      'summary': `${edu.studyType} ${edu.area}`,
      'link': {
        'name': edu.institution
      },
      'startDate': new Date(edu.startDate),
    };
    if(edu.endDate){
      line.endDate= new Date(edu.endDate);
    }
    if(edu.url){
      if(typeof line.link !== "undefined"){
        line.link.url = new URL(edu.url);
      }
    }
    if(edu.courses){
      line.details = edu.courses;
    }
    if(edu.score){
      if(Array.isArray(line.details)){
        line.details.push(`Moyenne : ${edu.score}`)
      }else{
        line.details = [`Moyenne : ${edu.score}`]
      }
    }
    line.id = await calcChecksum(line);
    lines.push(line);
  };
  return lines;
}

export async function fromCertificates(certificates: CERTIFICATE[]): Promise<Line[]>{
  let lines: Line[] = [];
  for(let cert of certificates){
    let line: Line = {
      'summary': cert.name,
      'link': {
        'name': cert.issuer
      },
      'endDate': new Date(cert.date),
    };
    line.id = await calcChecksum(line);
    lines.push(line);
  };
  return lines;
}

export async function fromWork(works: WORK[]): Promise<Line[]>{
  let lines: Line[] = [];
  for(let work of works){
    let line: Line = {
      'summary': work.position,
      'link': {
        'name': work.name
      },
      'startDate': new Date(work.startDate),
    };
    if(work.url){
      if(typeof line.link !== "undefined"){
        line.link.url = new URL(work.url);
      }
    }
    if(work.endDate){
      line.endDate = new Date(work.endDate);
    }
    if(work.highlights){
      line.details = [work.summary].concat(work.highlights);
    }
    line.id = await calcChecksum(line);
    lines.push(line);
  };
  return lines;
}

export async function fromSkills(skills: SKILL[], filter: string | undefined): Promise<Line[]>{
  let lines: Line[] = [];
  for (let skill of skills) {
    if(skill.category == filter){
      let line: Line = {
        'summary': skill.name,
        'details': []
      };
      if(skill.keywords){
        if(typeof line.details !== "undefined"){
          line.details.push(skill.keywords.join(', '));
        }
      }
      line.id = await calcChecksum(line);
      lines.push(line);
    }
  };
  return lines;
}

export async function fromInterests(interests: INTEREST[]): Promise<Line[]>{
  let lines: Line[] = [];
  for(let interest of interests){
    let line: Line = {
      'summary': interest.name,
      'details': []
    };
    if(interest.keywords){
      if(typeof line.details !== "undefined"){
        line.details.push(interest.keywords.join(', '));
      }
    }
    line.id = await calcChecksum(line);
    lines.push(line);
  };
  return lines;
}

export async function fromVolunteer(volunteers: VOLUNTEER[]): Promise<Line[]>{
  let lines: Line[] = [];
  for(let volunteer of volunteers){
    let line: Line = {
      'summary': (typeof volunteer.position === "undefined") ? "" : volunteer.position,
      'link': {
        'name': volunteer.organization
      },
      'startDate': new Date(volunteer.startDate)
    };
    if(volunteer.url){
      if(typeof line.link !== "undefined"){
        line.link.url = new URL(volunteer.url);
      }
    }
    if(volunteer.endDate){
      line.endDate = new Date(volunteer.endDate);
    }
    if(volunteer.highlights){
      line.details = volunteer.highlights;
    }
    line.id = await calcChecksum(line);
    lines.push(line);
  };
  return lines;
}

export async function fromLanguages(languages: LANGUAGE[]): Promise<Line[]>{
  let lines: Line[] = [];
  for(let language of languages){
    let line: Line = {
      'summary': language.language+" : "+language.fluency
    };
    line.id = await calcChecksum(line);
    lines.push(line);
  };
  return lines;
}

export async function fromProjects(projects: PROJECT[]): Promise<Line[]>{
  let lines: Line[] = [];
  for(let project of projects){
    let line: Line = {
      'summary': project.name,
      'details': [project.description].concat(project.highlights),
      'startDate': new Date(project.startDate),
      'link': {
        'name': (typeof project.entity === 'undefined' ) ? "" : project.entity,
        'url': new URL(project.url)
      }
    };
    if(line.link && line.link.name == "" && line.link.url && line.link.url.host){
      line.link.name = line.link.url.host.split('.').slice(-2).join('.')
    }
    if(project.endDate){
      line.endDate = new Date(project.endDate);
    }
    if(project.keywords && Array.isArray(project.keywords)){
      line.keywords = project.keywords;
    }
    line.id = await calcChecksum(line);
    lines.push(line);
  };
  return lines;
}
