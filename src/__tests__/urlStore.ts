/**
 * @jest-environment jsdom
 */

import persistentStorage from "../urlStore";

const sep="~"
const defaultUrl = "http://localhost/"
const key = "keyToTest"
const value = "valueTested"
const keyMultiValue = "keyWithMultipleValues"
const values = ["val1", "val2", "val3"]

function encodeValue(string:String):String{
    return encodeURI(string.replace('~', '%7E'));
}

beforeEach(() => {
    // reset url
    window.history.pushState(null, '', defaultUrl);
    // reset persistentStorage
    persistentStorage.resetFromCurrentUrl();
});

test('use jsdom in this test file', () => {
    const element = document.createElement('div');
    expect(element).not.toBeNull();
});

// getAsURL()
test('getAsURL() with nothing saved => good url', () => {
    expect(persistentStorage.getAsURL().toString()).toBe(
        defaultUrl
        );
});

test('getAsURL() with key and value => good url', () => {
    persistentStorage.add(key,value)
    expect(persistentStorage.getAsURL().toString()).toBe(
        defaultUrl+"?"+key+"="+value
        );
});

test('getAsURL() with key and values => good url', () => {
    persistentStorage.add(keyMultiValue,values[0])
    persistentStorage.add(keyMultiValue,values[1])
    persistentStorage.add(keyMultiValue,values[2])
    expect(persistentStorage.getAsURL().toString()).toBe(
        defaultUrl+"?"+keyMultiValue+"="+values[0]+sep+values[1]+sep+values[2]
    );
});

test('getAsURL() with keys and values => good url', () => {
    persistentStorage.add(keyMultiValue,values[0])
    persistentStorage.add(keyMultiValue,values[1])
    persistentStorage.add(keyMultiValue,values[2])
    persistentStorage.add(key,value)
    expect(persistentStorage.getAsURL().toString()).toBe(
        defaultUrl+"?"+keyMultiValue+"="+values[0]+sep+values[1]+sep+values[2]
        +"&"+key+"="+value
    );
});

// add(key: string, value: string)
test('add(string,string) => no return', () => {
    expect(persistentStorage.add(key,value)).toBeUndefined();
});

test('add(string,string) => set good url', () => {
    persistentStorage.add(key,value);
    expect(global.window.location.href).toContain(
        defaultUrl+"?"+key+"="+value
    );
});

test('add(string,string) => hasKeyVal', () => {
    persistentStorage.add(key,value);
    expect(persistentStorage.hasKeyVal(key,value)).toBeTruthy();
});

test('add(string,string) called multiple time with key and values => set good url', () => {
    persistentStorage.add(keyMultiValue,values[0])
    persistentStorage.add(keyMultiValue,values[1])
    persistentStorage.add(keyMultiValue,values[2])
    expect(global.window.location.href).toBe(
        defaultUrl+"?"+keyMultiValue+"="+values[0]+sep+values[1]+sep+values[2]
    );
});

test('add(string,string) called multiple time with keys and values => set good url', () => {
    persistentStorage.add(keyMultiValue,values[0])
    persistentStorage.add(keyMultiValue,values[1])
    persistentStorage.add(keyMultiValue,values[2])
    persistentStorage.add(key,value)
    expect(global.window.location.href).toBe(
        defaultUrl+"?"+keyMultiValue+"="+values[0]+sep+values[1]+sep+values[2]
        +"&"+key+"="+value
    );
});

test('add(string,string) called multiple time with same key and value => set good url', () => {
    persistentStorage.add(key,value)
    persistentStorage.add(key,value)
    persistentStorage.add(key,value)
    expect(global.window.location.href).toBe(
        defaultUrl+"?"+key+"="+value
    );
});

test('add(string,string) with value containing separator => set good url', () => {
    let valueWithSep = value+"_"+sep+"_"+value;
    persistentStorage.add(key,valueWithSep)
    // only test internal object, not the URL
    expect(global.window.location.href).toBe(
        defaultUrl+"?"+key+"="+value+"_"+encodeValue(sep)+"_"+value
    );
});

test('add(string,string) with value containing separator and other value => set good url', () => {
    let valueWithSep = value+sep+value;
    persistentStorage.add(key,valueWithSep);
    persistentStorage.add(key,value);
    // only test internal object, not the URL
    expect(global.window.location.href).toBe(
        defaultUrl+"?"+key+"="+value+encodeValue(sep)+value+sep+value
    );
});

// remove(key: string, value: string)
test('remove(string,string) with data => no return', () => {
    persistentStorage.add(key,value)

    expect(persistentStorage.remove(key,value)).toBeUndefined();
});

test('remove(string,string) without data => no return', () => {
    expect(persistentStorage.remove(key,value)).toBeUndefined();
});

test('remove(string,string) => set good url', () => {
    persistentStorage.add(key,value);

    persistentStorage.remove(key,value);

    expect(global.window.location.href).toContain(
        defaultUrl
    );
});

test('remove(string,string) called multiple time with key and values => set good url', () => {
    persistentStorage.add(keyMultiValue,values[0])
    persistentStorage.add(keyMultiValue,values[1])
    persistentStorage.add(keyMultiValue,values[2])

    persistentStorage.remove(keyMultiValue,values[0])
    persistentStorage.remove(keyMultiValue,values[1])
    persistentStorage.remove(keyMultiValue,values[2])

    expect(global.window.location.href).toBe(
        defaultUrl
    );
});

test('remove(string,string) called one time with key and values => set good url', () => {
    persistentStorage.add(keyMultiValue,values[0])
    persistentStorage.add(keyMultiValue,values[1])
    persistentStorage.add(keyMultiValue,values[2])

    persistentStorage.remove(keyMultiValue,values[0])

    expect(global.window.location.href).toBe(
        defaultUrl+"?"+keyMultiValue+"="+values[1]+sep+values[2]
    );
});

test('remove(string,string) called multiple time with keys and values => set good url', () => {
    persistentStorage.add(keyMultiValue,values[0])
    persistentStorage.add(keyMultiValue,values[1])
    persistentStorage.add(keyMultiValue,values[2])
    persistentStorage.add(key,value)

    persistentStorage.remove(keyMultiValue,values[2])
    persistentStorage.remove(key,value)

    expect(global.window.location.href).toBe(
        defaultUrl+"?"+keyMultiValue+"="+values[0]+sep+values[1]
    );
});

test('remove(string,string) called multiple time with same key and value => set good url', () => {
    persistentStorage.add(keyMultiValue,values[0])
    persistentStorage.add(keyMultiValue,values[1])
    persistentStorage.add(keyMultiValue,values[2])

    persistentStorage.remove(keyMultiValue,values[2])
    persistentStorage.remove(keyMultiValue,values[2])

    expect(global.window.location.href).toBe(
        defaultUrl+"?"+keyMultiValue+"="+values[0]+sep+values[1]
    );
});

test('remove(string,string) with value containing separator and remove value => set good url', () => {
    let valueWithSep = value+sep+value;
    persistentStorage.add(key,valueWithSep);
    persistentStorage.remove(key,value);
    expect(global.window.location.href).toBe(
        defaultUrl+"?"+key+"="+value+encodeValue(sep)+value
    );
});

// hasKeyVal(key: string, value: string|undefined): boolean
test('hasKeyVal(string,string) with value containing separator => is true', () => {
    let valueWithSep = value+sep+value;
    persistentStorage.add(key,valueWithSep)
    // only test internal object, not the URL
    expect(persistentStorage.hasKeyVal(key,valueWithSep)).toBeTruthy();
});

test('hasKeyVal(string,string) with value containing separator and one more char => is false', () => {
    let valueWithSep = value+sep+value;
    persistentStorage.add(key,valueWithSep)
    // only test internal object, not the URL
    expect(persistentStorage.hasKeyVal(key,valueWithSep+"1")).toBeFalsy();
});

test('hasKeyVal(string,string) with value containing separator => is false', () => {
    let valueWithSep = value+sep+value;
    persistentStorage.add(key,valueWithSep)
    // only test internal object, not the URL
    expect(persistentStorage.hasKeyVal(key,value)).toBeFalsy();
});

// resetFromCurrentUrl() test parsing url
test('resetFromCurrentUrl() with value => hasKey', () => {
    window.history.pushState(null, '', defaultUrl+"?"+key+"="+value);
    persistentStorage.resetFromCurrentUrl();
    expect(persistentStorage.hasKeyVal(key,value)).toBeTruthy();
});

test('resetFromCurrentUrl() with multiple value => hasKey true', () => {
    window.history.pushState(null, '', defaultUrl+"?"+keyMultiValue+"="+values[0]+sep+values[1]);
    persistentStorage.resetFromCurrentUrl();
    expect(persistentStorage.hasKeyVal(keyMultiValue,values[0])).toBeTruthy();
});

test('resetFromCurrentUrl() with value containing separator => hasKey true', () => {
    let valueWithSep = value+sep+value;
    window.history.pushState(null, '', defaultUrl+"?"+keyMultiValue+"="+encodeValue(valueWithSep));
    persistentStorage.resetFromCurrentUrl();
    expect(persistentStorage.hasKeyVal(keyMultiValue,valueWithSep)).toBeTruthy();
});