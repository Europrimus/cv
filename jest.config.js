/** @type {import('ts-jest').JestConfigWithTsJest} **/
export default {
  testEnvironment: "node",
  transform: {
    "^.+\\.ts$": [
      "ts-jest",
      {
        "useESM": true
        // optional: seperate tsconfig for tests
        //"tsconfig": "tsconfig.spec.json",
      }
    ],
    "^.+\\.svelte$": [
      "svelte-jester",
      {
        "preprocess": true
      }
    ],
  },
  "moduleFileExtensions": [
    "js",
    "ts",
    "svelte"
  ],
  "extensionsToTreatAsEsm": [
    ".svelte",
    ".ts"
  ]
};
